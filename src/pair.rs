use openssl::error::ErrorStack;
use openssl::pkey::{PKey, PKeyRef, Private};
use openssl::x509::{X509Ref, X509};

pub struct PrivateKey(pub PKey<Private>);

impl AsRef<PKeyRef<Private>> for PrivateKey {
    fn as_ref(&self) -> &PKeyRef<Private> {
        self.0.as_ref()
    }
}

pub struct Certificate(pub X509);

impl AsRef<X509Ref> for Certificate {
    fn as_ref(&self) -> &X509Ref {
        self.0.as_ref()
    }
}

pub struct CertPair {
    pub cert: Certificate,
    pub key: PrivateKey,
}

impl CertPair {
    pub fn new(cert: Certificate, key: PrivateKey) -> Result<CertPair, ErrorStack> {
        let c = CertPair { cert, key };
        Ok(c)
    }
}
