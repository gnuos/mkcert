use openssl::asn1::Asn1Time;
use openssl::ec::{EcGroup, EcKey};
use openssl::error::ErrorStack;
use openssl::hash::MessageDigest;
use openssl::nid::Nid;
use openssl::pkey::PKey;
use openssl::x509::extension::{
    //AuthorityKeyIdentifier,
    BasicConstraints,
    ExtendedKeyUsage,
    KeyUsage,
    SubjectKeyIdentifier,
};
use openssl::x509::{X509Builder, X509NameBuilder};

use std::fs;

use crate::bignum::generate_random_serial;
use crate::pair::{CertPair, Certificate, PrivateKey};

pub fn generate() -> Result<CertPair, ErrorStack> {
    let ec_group = EcGroup::from_curve_name(Nid::X9_62_PRIME256V1).unwrap();
    let ec_key = EcKey::generate(&ec_group).unwrap();

    let priv_pkey = PKey::from_ec_key(ec_key.clone()).unwrap();
    let pub_pkey = PKey::public_key_from_der(&priv_pkey.public_key_to_der().unwrap()).unwrap();

    let big = generate_random_serial().unwrap();
    let asn1_num = big.to_asn1_integer().unwrap();
    let not_before = Asn1Time::days_from_now(0).unwrap();
    let not_after = Asn1Time::days_from_now(365 * 10).unwrap();

    let mut x509name_builder = X509NameBuilder::new().unwrap();

    x509name_builder.append_entry_by_text("DC", "org").unwrap();
    x509name_builder
        .append_entry_by_text("DC", "rust-lang")
        .unwrap();
    x509name_builder
        .append_entry_by_text("CN", "Rust RootCA")
        .unwrap();
    let x509name = x509name_builder.build();
    let issuer = &x509name;
    let subject = &x509name;

    let mut x509_builder = X509Builder::new().unwrap();

    x509_builder.set_version(2).unwrap();
    x509_builder.set_serial_number(&asn1_num).unwrap();
    x509_builder.set_issuer_name(issuer).unwrap();
    x509_builder.set_subject_name(subject).unwrap();
    x509_builder.set_not_before(&not_before).unwrap();
    x509_builder.set_not_after(&not_after).unwrap();
    x509_builder.set_pubkey(&pub_pkey).unwrap();

    let x509_ctx = x509_builder.x509v3_context(None, None);

    let basic_constraints = BasicConstraints::new()
        .ca()
        .pathlen(1)
        .critical()
        .build()
        .unwrap();
    let key_usage = KeyUsage::new()
        .digital_signature()
        .key_cert_sign()
        .crl_sign()
        .critical()
        .build()
        .unwrap();
    let extended_key_usage = ExtendedKeyUsage::new()
        .server_auth()
        .client_auth()
        .code_signing()
        .time_stamping()
        .build()
        .unwrap();

    let subject_key_identifier = SubjectKeyIdentifier::new().build(&x509_ctx).unwrap();
    //let authority_key_identifier = AuthorityKeyIdentifier::new()
    //    .keyid(true)
    //    .issuer(false)
    //    .build(&x509_ctx)
    //    .unwrap();

    x509_builder.append_extension(basic_constraints).unwrap();
    x509_builder.append_extension(key_usage).unwrap();
    x509_builder.append_extension(extended_key_usage).unwrap();
    x509_builder
        .append_extension(subject_key_identifier)
        .unwrap();
    //x509_builder
    //    .append_extension(authority_key_identifier)
    //    .unwrap();

    x509_builder
        .sign(&priv_pkey, MessageDigest::sha256())
        .unwrap();

    let cert = x509_builder.build();

    let key_pem = ec_key.private_key_to_pem().unwrap();
    let cert_pem = cert.to_pem().unwrap();

    fs::write("/root/root-key.pem", key_pem).unwrap();
    fs::write("/root/root-cert.pem", cert_pem).unwrap();

    CertPair::new(Certificate(cert), PrivateKey(priv_pkey))
}
