use openssl::bn::{BigNum, MsbOption};
use openssl::error::ErrorStack;

pub fn generate_random_serial() -> Result<BigNum, ErrorStack> {
    let mut big = BigNum::new()?;

    big.rand(128, MsbOption::MAYBE_ZERO, true).unwrap();
    Ok(big)
}
