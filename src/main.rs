mod bignum;
mod client_cert;
mod pair;
mod rootca;
mod server_cert;

fn main() {
    let root_pair = rootca::generate().unwrap();

    server_cert::generate(&root_pair).unwrap();
    client_cert::generate(&root_pair).unwrap();
}
